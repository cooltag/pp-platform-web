import less from './stylesheets/app.less'

import Vue from 'vue'
import Resource from 'vue-resource'
import Router from 'vue-router'
import store from './store/index.js'

import ElementUI from 'element-ui'

import App from './pages/App.vue'
import Home from './pages/home/Home.vue'
import Bonus from './pages/bonus/Bonus.vue'

// Install plugins
Vue.use(Router)
Vue.use(Resource)
Vue.use(ElementUI)

// route config
let routes = [
    {
        path: '/home',
        name: 'home',
        component: Home
    },
    {
        path: '/bonus',
        name: 'bonus',
        component: Bonus 
    },
    { path: '*', redirect: '/home' }
]

// Set up a new router
let router = new Router({
    routes: routes
})

// Start up our app
new Vue({
    router: router,
    store,
    render: h => h(App)
}).$mount('#app')
