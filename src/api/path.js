import { PathResource } from './resources'

export default {
    getHomePath() {
        return PathResource.home();
    },
    getList(data) {
        return PathResource.list(null, data);
    },
    createDir (data) {
        return PathResource.create(null, data);
    }
}