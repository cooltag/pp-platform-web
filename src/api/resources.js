import Vue from 'vue'
import Resource from 'vue-resource'
import { Notification } from 'element-ui'

Vue.use(Resource)

const API_ROOT = process.env.NODE_ENV === 'production' ? '' : 'http://sbtest1.sbrella.in:40080';

Vue.http.interceptors.push((request, next) => {
    // 这里对请求体进行处理
    request.headers = request.headers || {};
    next((response) => {
        // 这里可以对响应的结果进行处理
        if (response.status !== 200) {
            Notification.error({
                title: '请求失败',
                message: '网络问题' + response.status,
                offset: 100
            });
        } else {
            if (response.body && response.body.code !== 200) {
                Notification.error({
                    title: '请求失败',
                    message: '网络问题' + response.body.errmsg,
                    offset: 100
                })
            }
        }
    })
})

const PROFILE_ROOT = '/projects/1/profiles{/id}';
const PATH_ROOT = '/path';

export const ProfileResource = Vue.resource(API_ROOT + PROFILE_ROOT, {}, {
    run: {method: 'post', url: API_ROOT + PROFILE_ROOT + '/run'},
    stop: {method: 'get', url: API_ROOT + PROFILE_ROOT + '/stop'},
    progresses: {method: 'get', url: API_ROOT + PROFILE_ROOT + '/progresses'},
    reportUrl: {method: 'get', url: API_ROOT + PROFILE_ROOT + '/url'}
});

export const PathResource = Vue.resource(API_ROOT + PATH_ROOT, {}, {
    home: {method: 'get', url: API_ROOT + PATH_ROOT + '/home'},
    list: {method: 'post', url: API_ROOT + PATH_ROOT + '/list'},
    create: {method: 'post', url: API_ROOT + PATH_ROOT + '/create'},
});