import { ProfileResource } from './resources'

export default {
    getProfile({id}) {
        return ProfileResource.get({id: id});
    },
    saveProfile({data}) {
        return ProfileResource.save(null, data);
    },
    updateProfile({id, data}) {
        return ProfileResource.save({id: id}, data);
    },
    run({id, data}) {
        return ProfileResource.run({id: id}, data);
    },
    progresses({id}) {
        return ProfileResource.progresses({id: id});
    },
    stop({id}) {
        return ProfileResource.stop({id: id});
    },
    getReportUrl({id}) {
        return ProfileResource.reportUrl({id: id});
    }
    
}