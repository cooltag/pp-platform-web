export default {
    deepClone(obj) {
        if (typeof obj !== 'object') {
            return obj;
        } else {
            return JSON.parse(JSON.stringify(obj));
        }
    },
    scrollbarToBottom() {
        let logBody = document.querySelector('#log');
        logBody.scrollTop = logBody.scrollHeight;
    },
    // 针对xx.xx形式的v-model采用添加watcher的方式commit，虽然这里commit只是多一个修改记录。
    // 其实真正被改了两次，v-model中被修改了才会被watcher监听到，然后再提交一次，这次会多一个记录，因为是vuex的commit，方便追踪
    // 当vuex的严格模式开启时，该方式会失效，因为不允许第一次的隐式修改，而如果禁掉第一次的隐式修改，这个watcher又不会触发
    // 所以不开严格模式其实没什么问题，因为所有修改会有commit记录，但是性能会差一些
    profileMapWatcher(paras) {
        let result = {};
        paras.forEach(function(para){
            if (para.indexOf('.') !== -1) {
                let mod = para.split('.')[0];
                let key = para.split('.')[1]; 
                result[para] = {
                    handler(newValue) {
                        this.$store.commit('profile/commitProfile', {
                            mod: mod,
                            key: key,
                            value: newValue 
                        });
                    },
                    deep: true
                }
            }
        });
        return result;
    },
    // 针对xx形式的v-model，vuex会直接禁止的双向绑定对值的修改，这里通过重写computed的set函数来做commit提交
    // 该方式在严格模式下是适用的
    // 那你问我为什么xx.xx形式不用这种方式来适配vuex的严格模式呢？
    // 原因是你定义的key为'xx.xx'的计算属性，在template中无法采用xx.xx的形式获取，当然可以转化为xx_xx这种形式
    // 但是如果你的xx.xx的key是参数的时候就完全不能用了,比如xx[key]，这种总不能转化为xx_key的形式了，对了，不要想不开用eval...
    profileMapState(paras) {
        let result = {};
        paras.forEach(function(key){
            result[key] = {
                get() {
                    return this.$store.state.profile[key];
                },
                set(value) {
                    this.$store.commit('profile/commitProfile', {
                        mod: null,
                        key: key,
                        value: value
                    })
                }
            }
        });
        return result;
    }
}