import Vue from 'vue'
import Vuex from 'vuex'
import profile from './modules/profile.js'
import task from './modules/task.js'

Vue.use(Vuex);

export default new Vuex.Store({
    modules: {
        profile,
        task
    },
    strict: false 
})