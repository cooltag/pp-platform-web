// profile
export const GET_PROFILE = 'GET_PROFILE';
export const SAVE_PROFILE = 'SAVE_PROFILE';

// task
export const SAVE_AND_RUN = 'SAVE_AND_RUN';
export const STOP_TASK = 'STOP_TASK';
export const GET_TASK_PROGRESS = 'GET_TASK_PROGRESS'



