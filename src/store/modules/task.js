import { Message } from 'element-ui'
import profile from 'src/api/profile'
import * as TASK_STATUS from 'src/constants/TaskStatus'

const state = {
    progress: '*',
    outputList: [],
    outputListShow: [],
    status: TASK_STATUS.NOT_RUN,
    reportUrl: null
};

const getters = {
};

const mutations = {
    commitProgress(state, data) {
        state.progress = data.progress;
        state.status = data.status;
        if (data.output && data.output.length > 0) {
            [].push.apply(state.outputList, data.output);
        }
    },
    clearOutputList(state) {
        state.outputList = [];
        state.outputListShow = [];
    },
    showOutput(state) {
        let addLine = function() {
            let item = state.outputList.shift();
            if (item) {
                if (item.overwrite) {
                    state.outputListShow.splice(state.outputListShow.length - 1, 1); // 去掉最后一项
                }
                state.outputListShow.push(item.line);
            }
            if (state.status === TASK_STATUS.RUNNING || state.outputListShow.length === 0 || state.outputList.length > 0) {
                let frequency = Math.min(1 / state.outputListShow.length * 1000, 100);
                setTimeout(addLine, frequency);
            }
        }
        addLine();
    },
    commitReportUrl(state, url) {
        if (url) {
            state.reportUrl = url;
        }
    }
};

const actions = {
    loadOutput({ commit, dispatch }) {
        commit('showOutput');
        dispatch('getProgresses');
    },
    getProgresses({ commit, dispatch }) {
        profile.progresses({id: 1}).then(function (res) {
            if (res.body.code === 200) {
                let data = res.body.data;
                commit('commitProgress', data);
                if (data.status === TASK_STATUS.RUNNING) { // 最后一次也是状态为0，1s后循环调用
                    setTimeout(() => {
                        dispatch('getProgresses');
                    }, 1000);
                } else if (data.status === TASK_STATUS.RUN_SUCCESS) {
                    dispatch('getReportUrl');
                }
            }
        });
    },
    getReportUrl({ commit }) {
        profile.getReportUrl({id: 1}).then(function(res){
            if (res.body.code === 200) {
                commit('commitReportUrl', res.body.data)
            }
        });
    },
    stopProfileTask({ commit }) {
        profile.stop({id: 1}).then(function(res) {
            if (res.body.code === 200) {
                Message.info({
                    title: '停止',
                    message: '任务已停止'
                });
            }
        })
    },
    clearLog({ commit }) {
        commit('clearOutputList');
    }
};

export default {
    namespaced: true,
    state,
    mutations,
    getters,
    actions
}
