import vue from 'vue'
import { Message } from 'element-ui'
import $ from 'jquery'

import profile from 'src/api/profile'
import * as types from 'src/store/mutation-types'
import util from 'src/services/util'
import ppCheckBaseConf from 'src/constants/PPCheckBaseConf'
import defaultProfile from 'src/constants/DefaultProfile'

export const COMMIT_PRPFILE = 'profile/COMMIT_PRPFILE'

const state = {
    ...util.deepClone(defaultProfile),
    used: {
        capture: false,
        check: false,
        report: false
    }
};

const mutations = {
    commitAllProfile (state, profile) {
        if (profile && Object.keys(profile).length === 0) { // 最初没有配置，则默认全开
            state.used = {
                capture: true,
                check: true,
                report: false
            };
            // console.log(state);
        } else if (profile) {
            Object.keys(state).forEach(key => { // 如果保存的有，则用保存的
                if (key in profile && profile[key]) {
                    if (typeof state[key] === 'object') {
                        $.extend(true, state[key], profile[key]);
                    } else {
                        state[key] = profile[key];
                    }
                    state.used[key] = true;
                }
            });
        }
    },
    commitProfile (state, { mod, key, value }) {
        console.log(typeof value, value, key);
        if (mod) {
            state[mod][key] = value;     
        } else {
            state[key] = value;
        }
    },
    commitChecked (state, {mod, checkedItems}) {
        console.log('check', mod, checkedItems);
        let check = state.check;
        ppCheckBaseConf[mod].items.forEach(function(item) {
            if (checkedItems.indexOf(item.key) !== -1) {
                check[item.key] = true;
            } else {
                check[item.key] = false;
            }
        });
    }
};

const actions = {
    getProfile({commit}) {
        return profile.getProfile({id: 1}).then(function(res){
            if (res.body.code === 200) {
                commit('commitAllProfile', res.body.data);
            }
        });
        // setTimeout(function(){
        //     commit('commitAllProfile', {capture: {include_source_paths: ['/yu']}, check: {limitations:{cpu_count: 2}}});
        // }, 4000)
    },
    saveProfile({state}) {
        let data = {
            build_dir: state.build_dir
        };

        ['capture', 'check', 'report'].forEach(key => { 
            if (state.used[key]) {
                data[key] = util.deepClone(state[key]);
            } else {
                data[key] = null;
            }
        });
        
        return profile.saveProfile({data: data}).then(function(res){
            if (res.body.code === 200) {
                Message.success({
                    title: '成功',
                    message: '保存成功'
                });
                return true;
            } else {
                return false;
            }
        });
    },
    runProfile({state, dispatch}) {  // run = save -> run -> progresses
        dispatch('task/clearLog', null, {root:true});
        return dispatch('saveProfile').then((next) => {
            return next && profile.run({id: 1, data: state.used}).then(res => {
                if (res.body.code === 200) {
                    return true;
                } else {
                    return false;
                }
            });
        }).then((next) => {
            return next && dispatch('task/loadOutput', null, { root: true })
        });
    }
};

const getters = {
    ppCheckConf(state) {
        let check = state.check;
        let modules = Object.keys(ppCheckBaseConf);
        let conf = {};
        modules.forEach(module => {
            let items = ppCheckBaseConf[module].items;
            conf[module] = {
                items: items,
                checkedItems: items.filter(item => check[item.key]).map(item => item.key)
            }
        });
        return conf;
    }
};

export default {
    namespaced: true,
    state,
    getters,
    actions,
    mutations
}

