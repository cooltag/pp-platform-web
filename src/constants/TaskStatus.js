export const NOT_RUN = 0;
export const RUNNING = 1;
export const RUN_SUCCESS = 2;
export const RUN_ERROR = 3;
export const RUN_STOP = 4;
// 0 未运行／ 1 正在运行 ／ 2 运行结束 成功 ／ 3 运行结束 错误 ／ 4 被停止