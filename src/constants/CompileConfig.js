export default [
    {
        "type": "path",
        "inputType": 'text',
        "label": "源代码目录",
        "placeholder": "/home/foo/src/",
        "mod": "capture",
        "key": "source_dir",
    },
    {
        "type": "path",
        "inputType": 'text',
        "label": "Pinpoint工作目录",
        "placeholder": "/home/foo/",
        "mod": null,
        "key": "build_dir"
    },
    {
        "type": "command",
        "inputType": 'textarea',
        "label": "编译命令",
        "placeholder": "configure",
        "mod": "capture",
        "key": "build",
        "hint": "$SOURCE_DIR 代表源代码目录，使用$BUILD_DIR 代表工作目录"
    },
    {
        "type": "command",
        "inputType": 'text',
        "label": "编译清理命令",
        "placeholder": "make clean",
        "mod": "capture",
        "key": "clean"
    },
];