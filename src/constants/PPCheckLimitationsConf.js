export default {
    'cpu_count': {
        name: 'CPU使用数量',
        placeholder: '1',
        type: 'integer',
        default: '',
    },
    'memory_size': {
        name: '内存限制（GB）',
        placeholder: '2（单位GB）',
        type: 'number',
        default: '',
        addOn: 'GB'
    },
    'timeout': {
        name: '分析时间限制',
        placeholder: '2（单位秒）',
        type: 'integer',
        default: 7200,
        addOn: '秒'
    },
    'thread_count': {
        name: '并行分析数',
        placeholder: '1',
        type: 'integer',
        default: 1,
    }
}