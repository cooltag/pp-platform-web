export default {
    build_dir: '', // 工作路径
    capture: {
        // make: '',
        // configure: '',
        clean: 'make clean',
        build: '$SOURCE_DIR/configure\nmake',
        source_dir: '', // 源代码路径
        include_source_paths: [], // 关注路径
        manual_include_source_paths: [], // 手动关注路径 
        // incremental: true // 复用以前的结果
    },
    check: {
        // 主要检查器
        ps_npd: true,
        ps_iusa: true,
        ps_uuv: true,
        ps_uaf: true,
        ps_fnhm: true,
        ps_taint: false,
        ssu_cnd: true,
        ssu_fdl: true,
        ssu_ml: true,

        // 实验检查器
        ps_bcis: false,
        ps_dbz: false,
        ps_icb: false,

        // 暂时废弃
        ps_fdl: false,
        ps_fdl2: false,
        ps_ml: false,
        ssu_dbf: false,

        inline_depth: 6,
        falcon_cg: false,

        run_csa: true,

        fast_analysis: false,

        limitations: {} // 运行限制，可缺省，缺省的话自动配置适当的参 
    },
    report: {
        project_name: 'example-project',
        server_addr: 'localhost',
        server_port: 40072,
        username: 'adminuser',
        password: 'adminuser'
    }
}
