export default {
    '主要检查器': {
        items: [
            {
                name: '空指针解引用',
                key: 'ps_npd',
                identifiers: [476, 690]
            },
            {
                name: '非法使用栈地址',
                key: 'ps_iusa',
                identifiers: [562, 590]
            },
            {
                name: '使用未初始化变量',
                key: 'ps_uuv',
                identifiers: [457]
            },
            {
                name: '内存释放后使用',
                key: 'ps_uaf',
                identifiers: [415, 416]
            },
            {
                name: '释放非堆内存',
                key: 'ps_fnhm',
                identifiers: [590]
            },
            {
                name: '空指针检查未覆盖解引用',
                key: 'ssu_cnd',
                identifiers: []
            },
            {
                name: '文件句柄泄漏',
                key: 'ssu_fdl',
                identifiers: [773, 775]
            },
            {
                name: '内存泄漏',
                key: 'ssu_ml',
                identifiers: [401]
            },
            {
                name: '污点分析（SQL注入，命令注入）',
                key: 'ps_taint',
                identifiers: [15, 90, 256, 319, 591, 23, 78, 123, 426, 427]
            },
        ]
    },
    '实验检查器': {
        items: [
            {
                name: '内存拷贝未检查有效长度',
                key: 'ps_bcis',
                identifiers: [120]
            },
            {
                name: '除零错误',
                key: 'ps_dbz',
                identifiers: [369]
            },
            {
                name: '缓冲区大小计算不正确',
                key: 'ps_icb',
                identifiers: [131]
            }
        ]
    }
}
