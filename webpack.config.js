var webpack = require('webpack');
var path = require('path');
var env = 'production';
// var env = 'development';

module.exports = {
    entry: [
        'babel-polyfill',
        './src/main.js'
    ],
    output: {
        path: "/dist/js",
        filename: "app.js"
    },
    watch: true,
    module: {
        loaders: [
            {
                test: /\.js$/,
                // excluding some local linked packages.
                // for normal use cases only node_modules is needed.
                exclude: /node_modules|vuex\/src/,
                loader: 'babel'
            },
            {
                test: /\.less$/,
                loaders: ['style', 'css', 'less']
            },
            {
                test: /\.css$/,
                loaders: ['style', 'css']
            },
            {
                test: /\.(gif|jpg|png|woff|svg|eot|ttf)\??.*$/,
                loader: 'url-loader?name=[path][name].[ext]'
            },
            {
                test: /\.vue$/,
                loader: 'vue'
            },
            {
                test: require.resolve('jquery'),
                loader: 'expose?JQuery!expose?$'
            }
        ]
    },
    babel: {
        presets: ['es2015', 'stage-3'],
        plugins: ['transform-runtime', 'transform-vue-jsx']
    },
    resolve: {
        modulesDirectories: ['node_modules'],
        alias: {
            'vue$': 'vue/dist/vue.common.js',
            'src': path.resolve(__dirname, 'src'),
        },
        extensions: ['', '.js']
    },
    plugins: [
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify(env) 
            }
        }),
        ...env === 'production' ? [ new webpack.optimize.UglifyJsPlugin()] : [],
    ]
}
